#!/bin/bash
username="cxg190009@"
domain=".utdallas.edu"
logs="logs/"
logextension=".log";

cat config.dat | sed -e "s/#.*//" | sed -e "/^\s*$/d" |
	{
		read line;
		numberOfRecords=$(echo $line | awk '{print $1}');
		echo "number of records to process: ""$numberOfRecords";

    clearCommand="ssh ${username}csgrads1${domain} 'rm -rf mexclusion/logs/*; rm -rf mexclusion/test/*; > clearlogs.info  &'";
    echo $clearCommand | bash -;

		while [[ $n -lt $numberOfRecords ]]
		do
			read line;
			node=$(echo $line | awk '{print $1}');
			host=$(echo $line | awk '{print $2}');
			port=$(echo $line | awk '{print $3}');
			query="ssh "${username}${host}${domain}" \"cd mexclusion; java -cp out/production/mexclusion edu.chetan.SctpNode ${node} > ${logs}node-${node}${logextension}  &\" " ;
			echo "executing command: "$query;
      echo $query | bash -
			n=$((n+1));
		done
		
	}
