package edu.chetan;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * A helper class required to log to a csv file.
 */
public class CriticalSectionLogWriter {

    /* CONSTANTS TO CREATE LOG FILE */
    private final String nodeId;
    private final String testFolder = "test";

    /* HOLDER LISTS OF LOG MESSAGES TO SUPPORT BUFFERING AND DEBUGGING */
    private final ArrayList<String> entryValue = new ArrayList<>();
    private final ArrayList<String> exitValue = new ArrayList<>();

    /**
     * Requires nodeId for which the log file is being created for
     * @param nodeId
     */
    public CriticalSectionLogWriter(String nodeId) {
        this.nodeId = nodeId;
    }

    /**
     * Adds a log message to critical section entry log
     * @param message
     */
    public void addCriticalSectionEntry(String message) {
        entryValue.add(message);
    }

    /**
     * Adds a log message to critical section exit log
     * @param message
     */
    public void addCriticalSectionExit(String message) {
        exitValue.add(message);
    }

    /**
     * Invoke this method to flush the records to file
     */
    public void writeToLog() {
        System.out.println("Log triggered, size: " + entryValue.size());
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(Paths.get(testFolder, "node-" + nodeId + ".csv"))) {
            for (int i = 0; i < entryValue.size(); i++) {
                bufferedWriter.write(entryValue.get(i));
                bufferedWriter.newLine();
                bufferedWriter.write(exitValue.get(i));
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
