package edu.chetan;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This helper class is used to parse the config file and pass on the information to the SCTP node class for processing
 */
public class ConfigParser {

    /* CONFIG FILE CONSTANTS */
    public static int numberOfNodes = 0;
    public static int requestGenerationDelay = 0;
    public static int numberOfRequestToGenerate = 0;
    public static int criticalSectionExecutionTime = 0;

    /* COUNTER TO CHECK THE PARSED RECORD COUNT */
    private static int numberOfRecordsProcessed = 0;

    /* HELPER CONSTANTS TO PARSE TO CONFIG FILE  */
    public static final String SPACE = " ";
    private static final String COMMENT = "#";
    private static final String UTD_DOMAIN = ".utdallas.edu";
    private static final String CONFIG_FILE_NAME = "config.dat";
    private static final HashMap<String, ArrayList<String>> nodeIdHostAndListenPortMap = new HashMap<>();

    /**
     * This getter method returns the map of all nodes with the host and port on which its deployed
     * Values in ArrayList with index values
     * Index 0 : hostname
     * Index 1 : port
     *
     * @return HashMap&lt;String, ArrayList&lt;String&gt;&gt;
     */
    public static HashMap<String, ArrayList<String>> getNodeIdHostAndListenPortMap() {
        return nodeIdHostAndListenPortMap;
    }

    /**
     * The main method which actually parses the config file
     */
    public static void processConfigurationFile() {
        try {
            List<String> configDataLines = Files.readAllLines(Paths.get(CONFIG_FILE_NAME));
            if (configDataLines.size() > 0) {
                String[] mainRecord = configDataLines.get(0).trim().split(COMMENT, 2)[0].trim().split(SPACE);
                numberOfNodes = Integer.parseInt(mainRecord[0]);
                requestGenerationDelay = Integer.parseInt(mainRecord[1]);
                criticalSectionExecutionTime = Integer.parseInt(mainRecord[2]);
                numberOfRequestToGenerate = Integer.parseInt(mainRecord[3]);


                configDataLines.stream().skip(1).limit(numberOfNodes)
                        .filter(configLine -> !configLine.isEmpty())
                        .filter(configLine -> Character.isDigit(configLine.charAt(0)))
                        .map(configLine -> {
                            if (configLine.contains(COMMENT)) {
                                return configLine.split(COMMENT, 2)[0].trim();
                            }
                            return configLine.trim();
                        })
                        .forEach(configLine -> {
                            String[] recordValues = configLine.split(SPACE);
                            if (recordValues.length < 3) {
                                System.out.println("Invalid config line, value tried to parse : " + configLine);
                                System.exit(0);
                            }
                            ArrayList<String> hostAndPortValues = new ArrayList<>();
                            if (recordValues[1].startsWith("dc"))
                                hostAndPortValues.add(recordValues[1] + UTD_DOMAIN);
                            else hostAndPortValues.add(recordValues[1]);
                            hostAndPortValues.add(recordValues[2]);

                            nodeIdHostAndListenPortMap.put(recordValues[0], hostAndPortValues);
                            numberOfRecordsProcessed++;

                        });

                if (numberOfNodes == 0 || numberOfRecordsProcessed < numberOfNodes) {
                    System.out.println("Insufficient number of entries in config file check record count, parsed records : " + numberOfRecordsProcessed);
                    System.exit(0);
                }

                System.out.println("map: " + nodeIdHostAndListenPortMap);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        ConfigParser.processConfigurationFile();
    }


}
