package edu.chetan;

import com.sun.nio.sctp.*;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.UnresolvedAddressException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeUnit;

import static edu.chetan.ConfigParser.*;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;

/**
 * An SCTP node which implements Roucairol and Carvalho mutual exclusion protocol for performing critical section
 * operations
 */
public class SctpNode {

    /*  NODE DEFAULTS   */
    private final String nodeId;
    private final int listenPort;

    /* HELPERS FOR CONNECTION AND HANDLING MESSAGES */
    private ByteBuffer receiveBuffer;
    private SelectionKey selectionKey;
    private Selector connectionSelector;
    private SctpServerChannel sctpServerChannel;
    private HashSet<String> duplicateMessageCheckSet;
    private SctpNotificationHandler sctpNotificationHandler;

    /* HELPER CLASS TO CREATE CSV LOG FILES WITH ENTRY AND EXIT TIMES*/
    private final CriticalSectionLogWriter criticalSectionLogWritter;

    /* CURRENT NODE VARIABLE VALUES */
    private long currentRequestTimeStamp;
    private final long currentNodeClock;

    private int replySentMessagesCount = 0;
    private int replyPendingCount = 0;
    private int replyReceivedCount = 0;
    private int numberOfRequestsGenerated = 0;
    private int currentProcessNumber = 1;

    /* APPLICATION DEFAULTS */
    private static boolean SHUTDOWN = false;
    private final int DEFAULT_SHUTDOWN_TIME_IN_MINUTES = 15;

    /* STANDARD HELPER MESSAGE CONSTANTS */
    private static final String REPLY = "REPLY";
    private static final String HELLO = "HELLO";
    private static final String REQUEST = "REQUEST";
    protected static final String ENTRY = "ENTRY";
    protected static final String EXIT = "EXIT";

    /* HELPER MAPS TO DEAL WITH CONNECTIONS */
    private final HashMap<String, SctpChannel> nodeIdChannelMap = new HashMap<>();
    private final Set<String> retryConnectionQueue = new ConcurrentSkipListSet<>();
    private final HashMap<Integer, String> associationAndNodeIdMap = new HashMap<>();
    private final HashMap<String, MessageInfo> nodeIdAndMessageInfoMap = new HashMap<>(); // nodeId and messageInfo containing remote port

    /* HELPER MAPS TO MANAGE CRITICAL SECTION PERMISSIONS */
    private final HashMap<String, Long> processRequestTimeMap = new HashMap<>();
    private final HashMap<String, Set<String>> processAndReplyMap = new HashMap<>();
    private final HashMap<String, Set<RequestContainer>> processWaitingReplyMap = new HashMap<>();

    /* EVALUATION METRICS */
    private Instant startingTime;
    private Instant endingTime;
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter
            .ofPattern("yyyy-MM-dd H:m:s:S").withZone(ZoneId.systemDefault());

    /**
     * The application requires nodeId and port for setup
     * @param nodeId The current id of the application node.
     * @param listenPort The port on which the node should listen for incoming connections
     */
    public SctpNode(String nodeId, int listenPort) {
        this.nodeId = nodeId;
        this.listenPort = listenPort;
        this.currentNodeClock = new Random().nextInt(1000);
        this.currentRequestTimeStamp = getCurrentClock() + currentNodeClock;
        this.criticalSectionLogWritter = new CriticalSectionLogWriter(nodeId);
        setupMaps();
    }

    /**
     * This helper method will return the current clock value of the node
     * @return current node clock
     */
    private long getCurrentClock() {

        return System.currentTimeMillis();
    }

    /**
     * This method is invoked to send REQUEST messages to all known hosts.
     */
    private void scheduleRequests() {
        if (numberOfRequestsGenerated < numberOfRequestToGenerate)
            sendRequestMessages();
    }

    /**
     * This method processes both the REQUEST and REPLY messages and also generates them when required
     * and on availability of necessary permission also invokes the critical-section execution
     */
    private void processCriticalSection() {

        startingTime = Instant.now();
        scheduleRequests();
        while (currentProcessNumber <= numberOfRequestToGenerate) {

            try {
                String currentProcessId = String.valueOf(currentProcessNumber);

                if (processWaitingReplyMap.get(currentProcessId).size() == nodeIdChannelMap.size()) {
                    while (processAndReplyMap.get(currentProcessId).size() != nodeIdChannelMap.size()) {

                        Set<RequestContainer> tempSet = new HashSet<>();
                        //send reply if lower clock is present
                        Set<RequestContainer> requestContainers = processWaitingReplyMap.get(currentProcessId);
                        requestContainers.forEach(waitingNode -> {
                            if (processRequestTimeMap.get(currentProcessId) > waitingNode.getRequestClock()) {

                                sendReplyMessage(waitingNode.nodeId, currentProcessId);
                                tempSet.add(waitingNode);
                            } else if (processRequestTimeMap.get(currentProcessId) == waitingNode.getRequestClock() && parseInt(nodeId) > parseInt(waitingNode.nodeId)) {
                                sendReplyMessage(waitingNode.nodeId, currentProcessId);
                                tempSet.add(waitingNode);
                            }
                        });
                        processWaitingReplyMap.get(currentProcessId).removeAll(tempSet);
                    }
                    cs_enter();
                    cs_leave();
                    TimeUnit.MILLISECONDS.sleep(requestGenerationDelay);
                    scheduleRequests();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        endingTime = Instant.now();
        printLogs();
    }

    /**
     * This method is specified in the documentation and is invoked when the application has received REPLY from
     * all the processes and performs critical-section operations.
     */
    private void cs_enter() {
        try {
            criticalSectionLogWritter.addCriticalSectionEntry(new StringJoiner(",")
                    .add(ENTRY)
                    .add(nodeId)
                    .add(String.valueOf(currentProcessNumber))
                    .add(String.valueOf(getCurrentClock())).toString());

            // System.out.println("Entering critical section for process : " + currentProcessNumber);
           // System.out.println("in critical section: " + processAndReplyMap.get(String.valueOf(currentProcessNumber)));
            TimeUnit.MILLISECONDS.sleep(criticalSectionExecutionTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method is specified in the documentation and is invoked when the application leaves the critical-section and
     * sends REPLY messages to waiting nodes.
     */
    private void cs_leave() {
        String currentProcessId = String.valueOf(currentProcessNumber);

        //System.out.println("leaving for number : " + currentProcessId);

        criticalSectionLogWritter.addCriticalSectionExit(new StringJoiner(",")
                .add(EXIT)
                .add(nodeId)
                .add(currentProcessId)
                .add(String.valueOf(getCurrentClock())).toString());

        try {
            processWaitingReplyMap.get(currentProcessId)
                    .forEach(waitingNode -> sendReplyMessage(waitingNode.nodeId, currentProcessId));
        } catch (Exception e) {
            System.out.println("Failed processWaitingReplyMap : " + processWaitingReplyMap);
        }
        // System.out.println("Exiting critical section for process : " + currentProcessId);
        currentProcessNumber++;
    }

    /**
     * This method starts listening on the specified port for incoming connections and also invokes connectToNeighbours
     * method to open connections to known hosts and also starts the critical-section operations
     * This is invoked on a new thread to avoid any blocking.
     */
    private void startServer() {
        try {
            connectionSelector = Selector.open();
            receiveBuffer = ByteBuffer.allocate(2048);
            duplicateMessageCheckSet = new HashSet<>();
            sctpNotificationHandler = new SctpNotificationHandler();

            sctpServerChannel = SctpServerChannel.open();
            sctpServerChannel.bind(new InetSocketAddress(listenPort));
            sctpServerChannel.configureBlocking(false);
            sctpServerChannel.register(connectionSelector, sctpServerChannel.validOps());

            //connect to neighbours
            new Thread(() -> {
                try {
                    connectToNeighbours();
                    while (!retryConnectionQueue.isEmpty()) connectToNeighbours();
                    processCriticalSection();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();

            System.out.println("Server Started");
            while (!SHUTDOWN) {
                connectionSelector.select(TimeUnit.MINUTES.toMillis(DEFAULT_SHUTDOWN_TIME_IN_MINUTES));
                Set<SelectionKey> selectionKeySet = connectionSelector.selectedKeys();
                Iterator<SelectionKey> selectionKeyIterator = selectionKeySet.iterator();

                while (selectionKeyIterator.hasNext()) {
                    selectionKey = selectionKeyIterator.next();

                    if (selectionKey.isAcceptable()) {
                        SctpChannel acceptedSctpChannel = sctpServerChannel.accept();
                        acceptedSctpChannel.configureBlocking(false);
                        acceptedSctpChannel.register(connectionSelector, acceptedSctpChannel.validOps());
                    }

                    if (selectionKey.isReadable()) {
                        handleReadMessage((SctpChannel) selectionKey.channel());
                        receiveBuffer.compact();
                        receiveBuffer.clear();
                    }

                    selectionKeyIterator.remove();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will connect to all the neighbours known to the application if they are not already connected.
     * Also failed connections are added to the retry-queue to try again for connection.
     * This method is invoked asynchronously in a new thread to avoid blocking.
     */
    private void connectToNeighbours() {
        HashMap<String, ArrayList<String>> nodeIdHostAndListenPortMap = ConfigParser.getNodeIdHostAndListenPortMap();
        String helloMessage = new StringJoiner(SPACE).add(HELLO).add(nodeId).toString();
        nodeIdHostAndListenPortMap.forEach((neighbourId, connectionInfo) -> {
            try {

                if (!nodeIdChannelMap.containsKey(neighbourId) && !nodeId.equals(neighbourId)) {
                    InetSocketAddress remoteNodeAddress = new InetSocketAddress(connectionInfo.get(0), parseInt(connectionInfo.get(1)));
                    SctpChannel sctpChannel = SctpChannel.open(remoteNodeAddress, 0, 0);
                    sctpChannel.configureBlocking(false);
                    sctpChannel.register(connectionSelector, sctpChannel.validOps());

                    associationAndNodeIdMap.put(sctpChannel.association().associationID(), neighbourId);
                    nodeIdChannelMap.put(neighbourId, sctpChannel);

                    MessageInfo messageInfo = MessageInfo.createOutgoing(sctpChannel.association(), null, 0);
                    nodeIdAndMessageInfoMap.put(neighbourId, messageInfo);

                    //send hello message
                    sctpChannel.send(ByteBuffer.wrap(helloMessage.getBytes()), messageInfo);

                    retryConnectionQueue.remove(neighbourId);
                }

            } catch (UnknownHostException | UnresolvedAddressException e) {
                e.printStackTrace();
                System.out.println(neighbourId + "has unresolvable address");
            } catch (ConnectException e) {
                System.out.println("ConnectionException when connecting to node : " + neighbourId);
                retryConnectionQueue.add(neighbourId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * This method is invoked when a channel becomes readable with data and is parsed accordingly.
     * This method also checks for duplicate messages and avoids processing them
     * @param sctpChannel The sctp non-blocking channel that has become available for reading data
     */
    private void handleReadMessage(SctpChannel sctpChannel) {
        try {
            sctpChannel.receive(receiveBuffer, System.out, sctpNotificationHandler);
            String receivedMessage = new String(receiveBuffer.array()).trim();

            if (!duplicateMessageCheckSet.contains(receivedMessage)) {
                duplicateMessageCheckSet.add(receivedMessage);
                //System.out.println(receivedMessage);

                String[] splitMessage = receivedMessage.split(SPACE);

                switch (splitMessage[0]) {
                    case REQUEST: {
                        processRequestMessage(splitMessage);
                        break;
                    }
                    case REPLY: {
                        processReplyMessage(splitMessage);
                        break;
                    }
                    case HELLO: {

                        if (!nodeIdChannelMap.containsKey(splitMessage[1])) {
                            nodeIdChannelMap.put(splitMessage[1], sctpChannel);
                            nodeIdAndMessageInfoMap.put(splitMessage[1], MessageInfo.createOutgoing(sctpChannel.association(), null, 0));
                            associationAndNodeIdMap.put(sctpChannel.association().associationID(), splitMessage[1]);

                        }
                        retryConnectionQueue.remove(splitMessage[1]);
                        break;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This helper method is used to parse the receiving REPLY messages.
     * Standard REPLY message format //REPLY <NODE-ID> <CLOCK> <PROCESS-ID>
     * @param replyMessage An array of strings containg the received message to be processed
     */
    private void processReplyMessage(String... replyMessage) {
        replyReceivedCount++;
        processAndReplyMap.get(replyMessage[3]).add(replyMessage[1]);
    }


    /**
     * This helper method parses the receiving REQUEST messages and stores them in RequestContainer to be processed
     * Standard format of the message // REQUEST <NODE-ID> <CLOCK> <PROCESS-ID>
     * @param requestMessage This is an array of strings of received message to be processed.
     */
    private void processRequestMessage(String... requestMessage) {
        processWaitingReplyMap.get(requestMessage[3]).add(new RequestContainer(Long.parseLong(requestMessage[2]), requestMessage[1]));
        replyPendingCount++;
    }

    /**
     * This method prepares a standard REPLY message and sends it to the specified node
     * Standard REPLY message format // REPLY <NODE-ID> <CLOCK> <PROCESS-ID>
     * @param nodeId The node to which the REPLY message should be sent
     * @param processId The process number to which the application is giving the permission to enter critical section
     */
    private void sendReplyMessage(String nodeId, String processId) {
        try {

            replySentMessagesCount++;

            String replyMessage = new StringJoiner(SPACE)
                    .add(REPLY)
                    .add(this.nodeId)
                    .add(String.valueOf(getCurrentClock()))
                    .add(processId).toString();
            // System.out.println("SENT: " + replyMessage);
            nodeIdChannelMap.get(nodeId)
                    .send(ByteBuffer.wrap(replyMessage.getBytes()), nodeIdAndMessageInfoMap.get(nodeId));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper method to setup all the hashmaps required for processing in the application
     */
    private void setupMaps() {
        for (int i = 1; i <= numberOfRequestToGenerate; i++) {
            processAndReplyMap.put(String.valueOf(i), new HashSet<>());
            processWaitingReplyMap.put(String.valueOf(i), new HashSet<>());
        }
    }

    /**
     * This helper method generates a standard request message and returns the generated message
     * Standard format is //REQUEST <NODE-ID> <CLOCK> <PROCESS-ID>
     * @return standard REQUEST message
     */
    private String prepareRequestMessage() {

        String requestCount = valueOf(++numberOfRequestsGenerated);
        currentRequestTimeStamp = getCurrentClock();

        processRequestTimeMap.put(requestCount, currentRequestTimeStamp);

        return new StringJoiner(SPACE)
                .add(REQUEST)
                .add(nodeId)
                .add(valueOf(currentRequestTimeStamp))
                .add(requestCount).toString();
    }

    /**
     * This method sends the REQUEST message for critical section to all the processes it is aware of
     */
    private void sendRequestMessages() {
        String requestMessage = prepareRequestMessage();
        nodeIdChannelMap.forEach((neighbourId, sctpChannel) -> {
            try {
                sctpChannel.send(ByteBuffer.wrap(requestMessage.getBytes()),
                        nodeIdAndMessageInfoMap.get(neighbourId));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static void main(String[] args) {

        if (args.length < 1) {
            System.out.println("Insufficient Arguments.");
            System.exit(0);
        }

        try {
            ConfigParser.processConfigurationFile();
            SctpNode sctpNode = new SctpNode(args[0], parseInt(ConfigParser.getNodeIdHostAndListenPortMap().get(args[0]).get(1)));
            Thread server = new Thread(sctpNode::startServer);
            server.start();
            CompletableFuture.runAsync(sctpNode::enableTimeOutShutDown).get();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * This method automatically shutdown the application after specified time has elapsed.
     */
    private void enableTimeOutShutDown() {
        try {
            System.out.println("Auto Shutdown Enabled");
            TimeUnit.MINUTES.sleep(DEFAULT_SHUTDOWN_TIME_IN_MINUTES);
            sctpServerChannel.close();
            printLogs();
            System.exit(0);
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    private void printLogs() {
        try {
            //PRINT THESE TWO VALUE TO CALCULATE THE RESPONSE TIME
            /*System.out.println("Process Request TimeStamps: " + processRequestTimeMap);
            System.out.println("processWaitingReplyMap: " + processWaitingReplyMap);*/

            SHUTDOWN = true;
            System.out.println("currentProcessNumber : " + currentProcessNumber);
            System.out.println("Shutting down node : " + nodeId + " due to time out");
            calculatePerformanceMetrics();
            criticalSectionLogWritter.writeToLog();
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculatePerformanceMetrics() {
        System.out.println("Entry Time : " + dateTimeFormatter.format(startingTime));
        System.out.println("Exit Time : " + dateTimeFormatter.format(endingTime));

        long elapsedDuration = endingTime.toEpochMilli()-startingTime.toEpochMilli();

        String metrics = new StringJoiner(":", "Elapsed Time Minutes:Seconds:MilliSeconds ", SPACE)
                .add(valueOf(TimeUnit.MILLISECONDS.toMinutes(elapsedDuration)))
                .add(valueOf(TimeUnit.MILLISECONDS.toSeconds(elapsedDuration)))
                .add(valueOf(TimeUnit.MILLISECONDS.toMillis(elapsedDuration))).toString();

        System.out.println(metrics);
    }

    /**
     * Notification Handler class removes the non-blocking connections the application is listening to when
     * the client shutdown
     */
    class SctpNotificationHandler extends AbstractNotificationHandler<PrintStream> {
        @Override
        public HandlerResult handleNotification(ShutdownNotification notification, PrintStream attachment) {
            attachment.printf("Removing association : %d of node : %s due to shutdown \n",
                    notification.association().associationID(), associationAndNodeIdMap.get(notification.association().associationID()));
            selectionKey.cancel();
            return super.handleNotification(notification, attachment);
        }
    }

    /**
     * A custom container class to hold all the pending requests the application has to respond to and
     * also used during validation process.
     */
    static class RequestContainer implements Comparable<RequestContainer> {
        private final long requestClock;
        private final String nodeId;
        private String processId;

        public RequestContainer(long requestClock, String nodeId) {
            this.requestClock = requestClock;
            this.nodeId = nodeId;
        }

        public RequestContainer(long requestClock, String nodeId, String processId) {
            this.requestClock = requestClock;
            this.nodeId = nodeId;
            this.processId = processId;
        }

        public long getRequestClock() {
            return requestClock;
        }

        public String getNodeId() {
            return nodeId;
        }


        @Override
        public String toString() {
            return new StringJoiner(", ", RequestContainer.class.getSimpleName() + "[", "]")
                    .add("requestClock=" + requestClock)
                    .add("nodeId='" + nodeId + "'")
                    .add( processId == null ? "" : "processId='" + processId + "'")
                    .toString();
        }

        @Override
        public int compareTo(RequestContainer requestContainer) {
            return Long.compare(requestClock, requestContainer.requestClock);
        }
    }
}
