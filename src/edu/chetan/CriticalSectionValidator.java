package edu.chetan;

import edu.chetan.SctpNode.RequestContainer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static edu.chetan.SctpNode.ENTRY;

/**
 * This class is used to validate if the mutual exclusion is enforced by using entry and exit time stamps
 */
public class CriticalSectionValidator {

    /* TEST FILE DEFAULTS  */
    private static final String TEST_FOLDER = "test";
    private static final String DEFAULT_TEST_FILE = "combined.csv";

    /* HELPER LISTS FOR VALIDATION */
    private ArrayList<RequestContainer> entryList = new ArrayList<>();
    private ArrayList<RequestContainer> exitList = new ArrayList<>();

    /**
     * This method performs the validation by checking if the entry and exit sequence is followed by all the nodes
     * then it checks for overlap of critical section.
     */
    private void performValidation() {
        try {
            if (!Files.exists(Paths.get(TEST_FOLDER, DEFAULT_TEST_FILE))) {
                System.out.println("Default test file not present, cannot validate");
                System.exit(0);
            }

            List<String> criticalSectionInfo = Files.readAllLines(Paths.get(TEST_FOLDER, DEFAULT_TEST_FILE));
            criticalSectionInfo.stream()
                    .map(record -> record.trim().split(","))
                    .filter(record -> record.length >= 3)
                    .forEach(record -> {
                        if (record[0].startsWith(ENTRY))
                            entryList.add(new RequestContainer(Long.parseLong(record[3]), record[1], record[2]));
                        else
                            exitList.add(new RequestContainer(Long.parseLong(record[3]), record[1], record[2]));
                    });

            List<RequestContainer> sortedEntryList = entryList.stream().sorted().collect(Collectors.toList());
            List<RequestContainer> sortedExitList = exitList.stream().sorted().collect(Collectors.toList());

            if (sortedEntryList.size() != sortedExitList.size()) {
                System.out.println("Validation failed size mismatch, entry size : " + sortedEntryList.size() + " exit size : " + sortedExitList.size());
                System.exit(0);
            }

            for (int i = 0; i < sortedEntryList.size(); i++) {
                if (!sortedEntryList.get(i).getNodeId().equals(sortedExitList.get(i).getNodeId())) {
                    System.out.println("Validation Failed, mismatch entry : "
                            + sortedEntryList.get(i) + " exit : " + sortedExitList.get(i));
                    System.exit(0);
                }

                if (i > 1 && sortedEntryList.get(i).getRequestClock() < sortedExitList.get(i - 1).getRequestClock()) {
                    System.out.println("Validation Failed, critical section overlap, entry: " + sortedEntryList.get(i)
                            + " exit: " + sortedExitList.get(i - 1));
                }
            }

            System.out.println("Validation Successful");


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        CriticalSectionValidator criticalSectionValidator = new CriticalSectionValidator();
        criticalSectionValidator.performValidation();
    }
}
