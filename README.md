# Roucairol and Carvalho’s Distributed Mutual Exclusion Algorithm

## Description  

The application performs mutual exclusion operation on a critical section
block using the **Roucairol and Carvalho** algorithm.

* The application has nodes which uses SCTP for communication and clocks must
be synchronized among various systems on which the application is deployed
to work properly.

* In case of out-of sync clock the critical section will be excceuted
correctly but cannot be validated using the provided validator.

* All the configuration should be placed in config.dat file at the application root.
If this name is changed recompile the "ConfigParser.java" with new name.

## Compiling

The application is based on Java 8 and requires JVM to run. No other dependencies, are needed.

* cd mexclusion
* javac -d out/production/mexclusion src/edu/chetan/ConfigParser.java
* javac -cp out/production/mexclusion -d out/production/mexclusion src/edu/chetan/CriticalSectionLogWriter.java
* javac -cp out/production/mexclusion -d out/production/mexclusion src/edu/chetan/CriticalSectionValidator.java
* javac -cp out/production/mexclusion -d out/production/mexclusion src/edu/chetan/SctpNode.java

**Required to store logs and for validation**

* mkdir test/
* mkdir logs/

## Executing 

The application node requires nodeId as program argument.

### Launching on localSystem
To launch a single node we can use the following command.
 
**Without log file**
* java -cp out/production/mexclusion/ edu.chetan.SctpNode 0 

**With log file**
* java -cp out/production/mexclusion/ edu.chetan.SctpNode 0 > logs/node-0.log & 

### Launching on dc machines.
Use the **"remotelauncher.sh"** script to launch nodes.

* make sure the dc servers have private/public key configured to avoid password prompt
* To load the key on localsystem use the following commands
    * ssh-agent $SHELL
    * ssh-add

## Output Files
The application will generate a ".csv" file for each node in the **test/** folder.
The csv file will contain the nodeId,process number,timestamp along with entry/exit type.

## Testing
To test the results aggregate all the .csv files into a sngle csv file
and run the **CriticalSectionValidator.java** to perform validation.

By default, the aggregate csv file is named as "combined.csv", if this is changed
make necessary changes in the java file and recompile it.

Only run the test after all the csv files are generated and application has terminated.

* cat test/node-*.csv >> test/combined.csv
* java -cp out/production/mexclusion/ edu.chetan.CriticalSectionValidator    


